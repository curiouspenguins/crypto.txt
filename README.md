# crypto.txt

A proposed standard that allows websites to define crypto addresses.

## Getting started

The idea behind Crypto.txt is straightforward: The organization places a file called crypto.txt in a predictable place — such as example.com/crypto.txt, or example.com/.well-known/crypto.txt. What’s in the crypto.txt is a list of crypto addresses.

## Example

You can see an example of a crypto.txt file here: https://curiouspenguins.com/.well-known/crypto.txt

## Installation
Place a file called crypto.txt in example.com/crypto.txt, or example.com/.well-known/crypto.txt. You can use the example above to replace the addresses with your own.

## Usage
List of websites using crypto.txt<br>
https://curiouspenguins.com/.well-known/crypto.txt<br>
https://www.thexyz.com/.well-known/crypto.txt<br>

## Contributing
Want to get invloved with this project? All you need it a GitLab account. 

## Authors and acknowledgment
ptoone.com<br>
curiouspenguin.xyz<br>

## License
We follow a Copyleft approach with software licensed under a GNU General Public License v3.0

## Project status
In development. 
